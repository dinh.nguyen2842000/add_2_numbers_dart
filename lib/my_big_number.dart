class MyBigNumber {
  String sum(String stn1, String stn2) {
    String str1 = stn1.replaceAll(RegExp(r'^0+(?=.)'), '');
    String str2 = stn2.replaceAll(RegExp(r'^0+(?=.)'), '');

    int lenStr1 = str1.length;
    int lenStr2 = str2.length;

    int maxLen = (lenStr1 > lenStr2) ? lenStr1 : lenStr2;

    String result = "";

    int rem = 0;

    int i1;
    int i2;

    int c1;
    int c2;
    int temp = 0;

    for (int i = 0; i < maxLen; i++) {
      i1 = lenStr1 - i - 1;
      i2 = lenStr2 - i - 1;

      c1 = (i1 >= 0) ? str1[i1].codeUnitAt(0) - 48 : 0;
      c2 = (i2 >= 0) ? str2[i2].codeUnitAt(0) - 48 : 0;

      temp = c1 + c2 + rem;

      result = (temp % 10).toString() + result;

      rem = temp ~/ 10;
    }
    if (rem > 0) result = rem.toString() + result;

    return result;
  }
}
