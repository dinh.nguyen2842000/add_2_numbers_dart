import 'package:add_2_numbers/my_big_number.dart';
import 'package:test/test.dart';

void main() {
  MyBigNumber myBigNumber = MyBigNumber();

  String sum1 = myBigNumber.sum('871263612873123', '999');
  String sum2 = myBigNumber.sum('99999999', '999999999999999999');
  String sum3 = myBigNumber.sum('0', '1998123');
  String sum4 = myBigNumber.sum('0', '0');
  String sum5 = myBigNumber.sum('', '123');
  String sum6 = myBigNumber.sum('-23', '123');
  String sum7 = myBigNumber.sum('auc', '123');

  test('Test1', () {
    expect("871263612874122", sum1);
  });
  test('Test2', () {
    expect("1000000000099999998", sum2);
  });
  test('Test3', () {
    expect("1998123", sum3);
  });
  test('Test4', () {
    expect("0", sum4);
  });
  // test('Test5', () {
  //   expect("0", sum5);
  // });
  // test('Test6', () {
  //   expect("100", sum6);
  // });
  // test('Test7', () {
  //   expect("", sum7);
  // });
}
