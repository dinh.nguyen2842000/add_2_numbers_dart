Clone project từ gitlab về máy

```bash
git clone https://gitlab.com/dinh.nguyen2842000/add_2_numbers_dart.git
```

## Chạy chương trình

```bash
> cd add_2_numbers
> dart run
```

Thư mục `/lib` chứa class `MyBigNumber`, bao gồm method `sum(stn1, stn2)`

unit test trong thư mục `/test`. Cách chạy test case:

```bash
> cd add_2_numbers
> dart test
```




